# Building #

## Debug build ##

```
 $ cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DBUILD_SHARED_LIBS=0
 $ cmake --build build
```

## Release build ##

```
 $ cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DBUILD_SHARED_LIBS=0
 $ cmake --build build
```
