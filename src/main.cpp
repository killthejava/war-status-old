#include <algorithm>
#include <array>
#include <iostream>

#include "cpr/parameters.h"
#include "helpers.hpp"
#include <fmt/format.h>

//
#include <cpr/cpr.h>
#include <sstream>
#include <string>

#include <html.hpp>
#include <string_view>

static constexpr std::string_view MAIN_SELECTOR = "div#connectivity-box-content";
static constexpr std::string_view GEMS_SELECTOR = "div.war-status-realm";
static constexpr std::string_view BUILDINGS_SELECTOR = "div.war-status-realm-buildings";

[[nodiscard]] std::array<Gem, TOTAL_GEMS> parse_gems_status(const html::node_ptr &container)
{
  auto gems_container = container->select(std::string(GEMS_SELECTOR));

  realm current_realm = realm::Alsius;
  std::array<Gem, TOTAL_GEMS> gems = get_gems();
  int gem_counter{};

  gems_container->walk([&current_realm, &gem_counter, &gems](html::node &n) {
    if (n.type_node == html::node_t::text) {
      auto content = n.to_text();
      trim(content);
      if (!content.empty()) {
        auto parts = tokenize_string(content, ' ');
        if (parts.size() == 3) {
          auto current = parts.back();
          if (current == "Alsius") {
            current_realm = realm::Alsius;
          } else if (current == "Ignis") {
            current_realm = realm::Ignis;
          } else if (current == "Syrtis") {
            current_realm = realm::Syrtis;
          }
        }
      }

      return false;
    }

    if (n.type_node == html::node_t::tag && n.tag_name == "img") {
      auto src = n.get_attr("src");
      if (!src.empty()) {
        auto parts = tokenize_string(src, '/');
        auto last = parts.back();
        if (last != "gem_0.png") {
          auto *gem = std::find_if(
            gems.begin(), gems.end(), [gem_counter](Gem &gem) { return gem.id_ == gem_counter % TOTAL_GEMS; });
          if (gem != gems.end()) {
            gem->owner_ = current_realm;
          }
        }

        gem_counter++;
      }

      return false;
    }

    return true;
  });


  return gems;
}

[[nodiscard]] std::array<Fort, TOTAL_FORTS> parse_buildings_status(const html::node_ptr &container)
{
  auto buildings_container = container->select(std::string(BUILDINGS_SELECTOR));
  std::array<Fort, TOTAL_FORTS> forts = get_forts();
  int fort_counter{};
  realm owner = realm::Alsius;

  buildings_container->walk([&forts, &fort_counter, &owner](html::node &n) {
    if (n.type_node == html::node_t::tag && n.tag_name == "img") {
      auto src = n.get_attr("src");

      if (!src.empty()) {
        auto parts = tokenize_string(src, '/');
        auto last = parts.back();

        if (last == "keep_alsius.gif") {
          owner = realm::Alsius;
        } else if (last == "keep_ignis.gif") {
          owner = realm::Ignis;
        } else if (last == "keep_syrtis.gif") {
          owner = realm::Syrtis;
        }
      }

      return false;
    }

    if (n.type_node == html::node_t::text) {
      auto content = n.to_text();
      trim(content);
      if (!content.empty()) {
        auto parts = tokenize_string(content, ' ');
        auto last = parts.back();

        std::stringstream ss;
        int id{};
        ss << last.substr(1, content.size() - 1);
        ss >> id;

        auto *fort = std::find_if(forts.begin(), forts.end(), [id](Fort &fort) { return fort.id_ == id; });
        if (fort != forts.end()) {
          fort->owner_ = owner;
        }
      }

      return false;
    }

    return true;
  });

  return forts;
}

[[nodiscard]] std::optional<std::array<Relic, TOTAL_RELICS>> parse_relics_status(const html::node_ptr &container)
{
  std::array<Relic, TOTAL_RELICS> relics = get_relics();
  bool found = false;

  container->walk([&found, &relics](html::node &n) {
    if (n.type_node == html::node_t::text) {
      return false;
    }

    if (n.type_node == html::node_t::tag && n.tag_name == "img") {
      auto title = n.get_attr("title");
      if (title.empty()) {
        return false;
      }
      found = true;
      auto src = n.get_attr("src");
      if (!src.empty()) {
        auto parts = tokenize_string(src, '/');
        auto last = parts.back();

        std::stringstream ss;
        int id{};
        ss << last.substr(4, 5);
        ss >> id;

        auto *relic = std::find_if(relics.begin(), relics.end(), [id](Relic &relic) { return relic.id_ == id; });
        if (relic != relics.end()) {
          relic->owned_ = true;
        }
      }

      return false;
    }

    if (n.type_node == html::node_t::tag && n.tag_name == "div") {
      auto classname = n.get_attr("class");
      if (classname == "box1-title" || classname == "war-status-realm-buildings") {
        return false;
      }
    }

    return true;
  });

  if (!found) {
    return std::optional<std::array<Relic, TOTAL_RELICS>>();
  }

  return relics;
}

void make_request(const std::string &world)
{
  cpr::Response r = cpr::Get(
    cpr::Url{ std::string(WAR_STATUS_URL) }, cpr::Parameters{ { "l", "1" }, { "sec", "3" }, { "world", world } });
  std::cout << r.status_code;
  std::cout << "\n";

  html::parser p;
  html::node_ptr node = p.parse(r.text);

  auto container = node->select(std::string(MAIN_SELECTOR));
  auto gems = parse_gems_status(container);

  for (const Gem &gem : gems) {
    std::cout << "Gem " << gem.id_ << ": ";
    if (gem.owner_ == realm::Alsius) {
      std::cout << "Alsius";
    } else if (gem.owner_ == realm::Ignis) {
      std::cout << "Ignis";
    } else if (gem.owner_ == realm::Syrtis) {
      std::cout << "Syrtis";
    }
    std::cout << "\n";
  }

  auto buildings = parse_buildings_status(container);

  for (const Fort &fort : buildings) {
    std::cout << "Fort " << static_cast<std::string>(fort) << ": ";

    if (fort.owner_ == realm::Alsius) {
      std::cout << "Alsius";
    } else if (fort.owner_ == realm::Ignis) {
      std::cout << "Ignis";
    } else if (fort.owner_ == realm::Syrtis) {
      std::cout << "Syrtis";
    }

    std::cout << "\n";
  }

  auto maybe_relics = parse_relics_status(container);
  if (!maybe_relics.has_value()) {
    std::cout << "No hay reliquias :S\n";
    return;
  }

  for (const auto &relic : maybe_relics.value()) {
    std::cout << "Reliquia " << static_cast<std::string>(relic) << ": ";
    std::cout << (relic.owned_ ? "OK" : "-");
    std::cout << "\n";
  }
}

int main()
{
  make_request("ra");
  // make_request("valhalla");
}
