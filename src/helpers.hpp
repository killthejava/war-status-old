#pragma once

#include <array>
#include <string>
#include <string_view>
#include <vector>

static constexpr std::string_view WAR_STATUS_URL = "https://www.championsofregnum.com/index.php?l=0&sec=3";

enum class realm { Syrtis, Ignis, Alsius };

constexpr int TOTAL_GEMS = 6;
constexpr int TOTAL_RELICS = 9;
constexpr int TOTAL_FORTS = 12;

struct Gem
{
  int id_;
  realm realm_;
  realm owner_;
};

enum class fort {
  //
  Algaros,
  Herbred,
  Eferias,
  SyrtisWall,
  //
  Menirah,
  Samal,
  Shaanarid,
  IgnisWall,
  //
  Aggersborg,
  Trelleborg,
  Imperia,
  AlsiusWall
};

struct Relic
{
  int id_;
  fort fort_;
  realm realm_;
  bool owned_;

  explicit operator std::string() const
  {
    switch (fort_) {
    case fort::Aggersborg:
      return "Aggersborg";

    case fort::Algaros:
      return "Algaros";

    case fort::Eferias:
      return "Eferias";

    case fort::Herbred:
      return "Herbred";

    case fort::Imperia:
      return "Imperia";

    case fort::Menirah:
      return "Menirah";

    case fort::Samal:
      return "Samal";

    case fort::Shaanarid:
      return "Shaanarid";

    case fort::Trelleborg:
      return "Trelleborg";

    case fort::AlsiusWall:
      return "Alsius";

    case fort::IgnisWall:
      return "Ignis";

    case fort::SyrtisWall:
      return "Syrtis";
    }

    return "";
  }
};

struct Fort
{
  int id_;
  fort name_;
  realm realm_;
  realm owner_;
  bool is_wall_ = false;

  explicit operator std::string() const
  {
    switch (name_) {
    case fort::Aggersborg:
      return "Aggersborg";

    case fort::Algaros:
      return "Algaros";

    case fort::Eferias:
      return "Eferias";

    case fort::Herbred:
      return "Herbred";

    case fort::Imperia:
      return "Imperia";

    case fort::Menirah:
      return "Menirah";

    case fort::Samal:
      return "Samal";

    case fort::Shaanarid:
      return "Shaanarid";

    case fort::Trelleborg:
      return "Trelleborg";

    case fort::AlsiusWall:
      return "Alsius";

    case fort::IgnisWall:
      return "Ignis";

    case fort::SyrtisWall:
      return "Syrtis";
    }

    return "";
  }
};


[[nodiscard]] constexpr std::array<Gem, TOTAL_GEMS> get_gems()
{
  std::array<Gem, TOTAL_GEMS> gems = {
    Gem{ 0, realm::Syrtis, realm::Syrtis },
    Gem{ 1, realm::Alsius, realm::Alsius },
    Gem{ 2, realm::Ignis, realm::Ignis },
    Gem{ 3, realm::Alsius, realm::Alsius },
    Gem{ 4, realm::Ignis, realm::Ignis },
    Gem{ 5, realm::Syrtis, realm::Syrtis },
  };

  return gems;
}

[[nodiscard]] constexpr std::array<Fort, TOTAL_FORTS> get_forts()
{
  std::array<Fort, TOTAL_FORTS> forts = {
    Fort{ 1, fort::Imperia, realm::Alsius, realm::Alsius },
    Fort{ 2, fort::Aggersborg, realm::Alsius, realm::Alsius },
    Fort{ 3, fort::Trelleborg, realm::Alsius, realm::Alsius },
    Fort{ 4, fort::AlsiusWall, realm::Alsius, realm::Alsius, true },
    //
    Fort{ 5, fort::Menirah, realm::Ignis, realm::Ignis },
    Fort{ 6, fort::Samal, realm::Ignis, realm::Ignis },
    Fort{ 7, fort::Shaanarid, realm::Ignis, realm::Ignis },
    Fort{ 8, fort::IgnisWall, realm::Ignis, realm::Ignis, true },
    //
    Fort{ 9, fort::Algaros, realm::Syrtis, realm::Syrtis },
    Fort{ 10, fort::Herbred, realm::Syrtis, realm::Syrtis },
    Fort{ 11, fort::Eferias, realm::Syrtis, realm::Syrtis },
    Fort{ 12, fort::SyrtisWall, realm::Syrtis, realm::Syrtis, true },
  };
  return forts;
}

[[nodiscard]] constexpr std::array<Relic, TOTAL_RELICS> get_relics(const bool owned = false)
{
  std::array<Relic, TOTAL_RELICS> relics = {
    //
    Relic{ 79167, fort::Imperia, realm::Alsius, owned },
    Relic{ 79168, fort::Aggersborg, realm::Alsius, owned },
    Relic{ 79174, fort::Trelleborg, realm::Alsius, owned },
    //
    Relic{ 79170, fort::Menirah, realm::Ignis, owned },
    Relic{ 79169, fort::Shaanarid, realm::Ignis, owned },
    Relic{ 79171, fort::Samal, realm::Ignis, owned },
    //
    Relic{ 79172, fort::Eferias, realm::Syrtis, owned },
    Relic{ 79175, fort::Herbred, realm::Syrtis, owned },
    Relic{ 79173, fort::Algaros, realm::Syrtis, owned },
  };

  return relics;
}

[[nodiscard]] std::vector<std::string> tokenize_string(const std::string &, const char);

// trim from start (in place)
static inline void ltrim(std::string &s)
{
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) { return !std::isspace(ch); }));
}

// trim from end (in place)
static inline void rtrim(std::string &s)
{
  s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) { return !std::isspace(ch); }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s)
{
  ltrim(s);
  rtrim(s);
}
