#include "helpers.hpp"
#include <array>
#include <sstream>
#include <utility>

[[nodiscard]] std::vector<std::string> tokenize_string(const std::string &string_to_split, const char sep)
{
  std::vector<std::string> result;
  std::stringstream ss(string_to_split);
  std::string temp_token;
  while (std::getline(ss, temp_token, sep)) {
    result.emplace_back(std::move(temp_token));
  }

  return result;
}
